# Linear Layers in P-SPN and HADES-Like Permutations
This repository contains a tool to evaluate the security of a linear layer in the context of P-SPN and HADES-like permutations. It is based on the results provided in [[1]](#1).

## Included files
Two script files are included in the `code` folder:
- `algorithms_gf2n.sage` contains the implementation for binary fields.

- `algorithms_gfp.sage` contains the implemnetation for prime fields.

Both files contain Algorithm 1, Algorithm 2, Algorithm 3, and various utility functions.

## Usage
To run the tests, use
`sage <script> <n> <t> <sample_size> <seed>`

`n` is size of the field (n = ceil(log_2(p)) in prime fields)

`t` for a t x t matrix

`sample_size` specifies the number of matrices to test

`seed` is used for the pseudo-random generation of matrices

By default, the algorithms then start various tests. Specifically, they test STARKAD and POSEIDON matrices, random invertible matrices, and random Cauchy matrices.

## Testing Examples Matrices

To test the Zorro matrix or the examples given in Section 5.2.1, Section 5.3, and Appendix F.1, set the parameter `single_matrix_test` to `True` at the top of the script file. Note that some examples work only in a respective field (e.g., testing the Zorro matrix is only done in algorithms_gf2n.sage) and for certain parameters (e.g., `s=4` for the Zorro matrix).

## References
<a id="1">[1]</a> L. Grassi, C. Rechberger, M. Schofnegger. *Proving Resistance Against Infinitely Long Subspace Trails: How to Choose the Linear Layer*. FSE/ToSC, Volume 2021, Issue 2. 2021. Online: https://eprint.iacr.org/2020/500