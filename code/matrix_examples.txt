=== Zorro ===
[    x     0     0     0     0 x + 1     0     0     0     0     1     0     0     0     0     1]
[    0     x     0     0     0     0 x + 1     0     0     0     0     1     1     0     0     0]
[    0     0     x     0     0     0     0 x + 1     1     0     0     0     0     1     0     0]
[    0     0     0     x x + 1     0     0     0     0     1     0     0     0     0     1     0]
[    1     0     0     0     0     x     0     0     0     0 x + 1     0     0     0     0     1]
[    0     1     0     0     0     0     x     0     0     0     0 x + 1     1     0     0     0]
[    0     0     1     0     0     0     0     x x + 1     0     0     0     0     1     0     0]
[    0     0     0     1     x     0     0     0     0 x + 1     0     0     0     0     1     0]
[    1     0     0     0     0     1     0     0     0     0     x     0     0     0     0 x + 1]
[    0     1     0     0     0     0     1     0     0     0     0     x x + 1     0     0     0]
[    0     0     1     0     0     0     0     1     x     0     0     0     0 x + 1     0     0]
[    0     0     0     1     1     0     0     0     0     x     0     0     0     0 x + 1     0]
[x + 1     0     0     0     0     1     0     0     0     0     1     0     0     0     0     x]
[    0 x + 1     0     0     0     0     1     0     0     0     0     1     x     0     0     0]
[    0     0 x + 1     0     0     0     0     1     1     0     0     0     0     x     0     0]
[    0     0     0 x + 1     1     0     0     0     0     1     0     0     0     0     x     0]
Field: GF(2^8)
Result Algorithm 1 (expected: True): [True, 0]
Result Algorithm 2 (expected: True): [True, None]
Result Algorithm 3 (expected: True): [True, None]

=== Invariant Subspace Trails with Active S-Boxes ===
[  0 150  72  32]
[ 92   9 122  66]
[ 11  96  31  87]
[140  35 138  43]
Field: GF(163)
Result (expected: False): [False, [Vector space of degree 4 and dimension 2 over Finite Field of size 163
Basis matrix:
[  1   0   0   0]
[  0   1 133 122], [0]]]
Spaces match: True

=== Iterative Subspace Trails with Active S-Boxes, but no Invariant Ones ===
[  0   1 222]
[  1 221   1]
[  1 219   2]
Field: GF(223)
Result (expected: True): [True, 0]
Result (expected: True): [True, None]
Result (expected: False): [False, [Vector space of degree 3 and dimension 1 over Finite Field of size 223
Basis matrix:
[1 0 0], None, 3]]

=== Invariant Subspace Trails with Active S-Boxes (Generalization) ===
[  1 143  14  64]
[ 63 122  83 172]
[ 24 103  70  46]
[117 108  89 150]
Field: GF(181)
Result (expected: False): [False, [Vector space of degree 4 and dimension 2 over Finite Field of size 181
Basis matrix:
[  1   0   0   0]
[  0   1   9 157], [0]]]
Spaces match: True
